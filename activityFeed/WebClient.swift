//
//  WebClient.swift
//  activityFeed
//
//  Created by Ge Huang on 3/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

typealias baseRequestCompletionBlock = (Data?, Error?, @escaping ItemsResultCompletionBlock) -> Void
typealias urlRequestCompletionBlock = (Data?, Error?, @escaping ImageCompletionBlock) -> Void

class WebClient {

	private static let baseURLString = "https://api.rss2json.com/v1/api.json"
	private static let rssURLString = "http://www.abc.net.au/news/feed/51120/rss.xml"

	public static let shared: WebClient = WebClient()

	private let session: URLSession = {
		let config = URLSessionConfiguration.default
		return URLSession(configuration: config)
	}()

	static var baseRemoteURL: URL {
		return remoteURL(parameters: ["rss_url": rssURLString])
	}

	static func remoteURL(parameters: [String:String]?) -> URL {
		var components = URLComponents(string: baseURLString)!
		var queryItems = [URLQueryItem]()
		if let additionalParams = parameters {
			for (key, value) in additionalParams {
				let item = URLQueryItem(name: key, value: value)
				queryItems.append(item)
			}
		}
		components.queryItems = queryItems
		return components.url!
	}

	// request data from remote base URL
	func baseRequest(completion: @escaping baseRequestCompletionBlock, resultCompletion: @escaping ItemsResultCompletionBlock) {
		let request = URLRequest(url: WebClient.baseRemoteURL)
		let task = session.dataTask(with: request) {
			(data, response, error) in
			completion(data,error, resultCompletion)
		}
		task.resume()
	}

	// request data from remote specific URL
	func urlRequest(_ url: URL, completion: @escaping urlRequestCompletionBlock, resultCompletion: @escaping ImageCompletionBlock) {
		let request = URLRequest(url: url)
		let task = session.dataTask(with: request) {
			(data, response, error) in
			completion(data,error, resultCompletion)
		}
		task.resume()
	}
}
