//
//  JSONParser.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import Foundation

struct JSONParser {
	
	// date formatter to interpret input data
	static let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
		return formatter
	}()

	// parse JSON to array of items
	static func items(fromJSON data: Data) -> ItemsResult {

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase
		// provide a custom dateFormatter for decoding
		decoder.dateDecodingStrategy = .formatted(dateFormatter)

		do {
			let response: ActivityItemsResponse = try decoder.decode(ActivityItemsResponse.self, from: data)
			return .success(response.items)
		}
		catch (let error) {
			return .failure(error)
		}
	}
}
