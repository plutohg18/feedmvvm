//
//  AppDelegate.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	// shortcut to ActivityViewController
	func activityViewController() -> ActivityViewController {
		let rootViewController = window!.rootViewController as! UINavigationController
		return rootViewController.topViewController as! ActivityViewController
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		activityViewController().refresh()
	}
}

