//
//  ActivityItem.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import Foundation

struct ActivityItem: Decodable {
	let title: String
	let pubDate: Date
	let link: URL
	let enclosure: Enclosure?
	let thumbnail: URL?
}

struct ActivityItemsResponse: Decodable {
	let items: [ActivityItem]
}

struct Enclosure: Decodable {
	let link: String
}
