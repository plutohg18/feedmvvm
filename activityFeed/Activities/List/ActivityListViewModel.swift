//
//  ActivityListViewModel.swift
//  activityFeed
//
//  Created by Ge Huang on 23/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

class ActivityListViewModel {
	private let fetcher: ActivityDataFetcher
	private var items = [ActivityItem]()

	var refreshHandler: (()->())?

	init(_ fetcher: ActivityDataFetcher) {
		self.fetcher = fetcher
	}

	func itemsCount() -> Int {
		return items.count
	}

	func itemForRow(_ row : Int) -> ActivityItem {
		return items[row]
	}

	func cellViewModelForRow(_ row : Int, fetcher: ActivityDataFetcher) -> ActivityCellViewModel  {
		return ActivityCellViewModel(items[row], fetcher: fetcher, isFirstItem: row == 0)
	}

	func refreshData() {
		fetcher.fetchItems {
			[unowned self] photosResult in
			switch photosResult {
			case let .success(items):
				self.items = items
				self.refreshHandler?()
			case let .failure(error):
				print("Error fetching items: \(error)")
			}
		}
	}

	// the same logic as refresh, separate it out for testing purpose
	func handleRefreshControl() {
		refreshData()
	}
}
