//
//  ActivityViewController.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

class ActivityViewController: UITableViewController {

	static let kTopCellIdentifier = "topCell"
	static let kRegularCellIdentifier = "regularCell"

	private lazy var viewModel: ActivityListViewModel = {
		return ActivityListViewModel(dataFetcher)
	}()

	private lazy var dataFetcher : ActivityDataFetcher = {
		return ActivityDataFetcher(webClient: WebClient.shared)
	}()

	override func viewDidLoad() {
        super.viewDidLoad()
		// add logo to navigation bar
		let logo = UIImage(named: "logo.png")
		let imageView = UIImageView(image:logo)
		self.navigationItem.titleView = imageView

		// allowing auto-sizing table view
		tableView.rowHeight = UITableView.automaticDimension
		tableView.estimatedRowHeight = 265

		initializeViewModel()

		// refresh data
		refresh()
    }

	func initializeViewModel() {
		// intialize callbacks
		viewModel.refreshHandler =  {
			[unowned self] in
			self.tableView.reloadData()

			// dismis refresh control if needed
			self.dismissRefreshControl()
		}
	}

	func refresh() {
		viewModel.refreshData()
	}
}

// MARK: - Table view data source
extension ActivityViewController {
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.itemsCount()
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		// return top cell for fisrt row and regular cell for the rest of table
		let identifier = indexPath.row == 0 ? ActivityViewController.kTopCellIdentifier : ActivityViewController.kRegularCellIdentifier
		let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ActivityCell
		let cellViewModel = viewModel.cellViewModelForRow(indexPath.row, fetcher: dataFetcher)
		cell.configureWith(cellViewModel)

		return cell
	}	
}

// MARK: - Push to refresh
extension ActivityViewController {
	@IBAction func handleRefresh(_ sender: UIRefreshControl){
		viewModel.handleRefreshControl()
	}

	private func dismissRefreshControl() {
		// dismis refresh control if needed
		if let refreshControl = self.refreshControl, refreshControl.isRefreshing {
			refreshControl.endRefreshing()
		}
	}
}

// MARK: - Navigation
extension ActivityViewController {
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segue.identifier {
		case "showWebView":
			if let selectedIndexPath =
				tableView.indexPathsForSelectedRows?.first {
				let item = viewModel.itemForRow(selectedIndexPath.row)
				let destination = segue.destination as! WebViewController
				destination.linkURL = item.link
			}
		default:
			preconditionFailure("Unexpected segue identifier.")
		}
	}
}
