//
//  ActivityDataFetcher.swift
//  activityFeed
//
//  Created by Ge Huang on 2/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

enum ItemError: Error {
	case imageCreationError
}

enum ItemsResult {
	case success([ActivityItem])
	case failure(Error)
}
enum ImageResult {
	case success(UIImage)
	case failure(Error)
}

typealias ImageCompletionBlock = (ImageResult) -> Void
typealias ItemsResultCompletionBlock = (ItemsResult) -> Void

class ActivityDataFetcher {
	private let webClient: WebClient
	
	init(webClient: WebClient) {
		self.webClient = webClient
	}

	// fetch items from remote URL
	func fetchItems(completion: @escaping ItemsResultCompletionBlock) {
		webClient.baseRequest(completion: self.processItemsRequest, resultCompletion: completion)
	}

	// process JSON Data to create items and complete to update UI
	private func processItemsRequest(data: Data?, error: Error?, completion: @escaping (ItemsResult) -> Void) {
		guard let jsonData = data else {
			return completion(.failure(error!))
		}
		// make sure update happens on main thread
		OperationQueue.main.addOperation {
			completion(JSONParser.items(fromJSON:jsonData))
		}
	}

	// fetch individual image from remote URL
	func fetchImage(_ imageURL: URL, completion: @escaping ImageCompletionBlock) {
		webClient.urlRequest(imageURL, completion: self.processImageRequest, resultCompletion: completion)
	}

	// process Data to generate image and complete to update UI
	private func processImageRequest(data: Data?, error: Error?, completion: @escaping (ImageResult) -> Void) {
		guard let imageData = data, let image = UIImage(data: imageData) else {
			// Couldn't create an image
			if data == nil {
				completion(.failure(error!))
			} else {
				completion(.failure(ItemError.imageCreationError))
			}
			return
		}
		// make sure update happens on main thread
		OperationQueue.main.addOperation {
			completion(.success(image))
		}
	}
}
