//
//  ActivityCellViewModel.swift
//  activityFeed
//
//  Created by Ge Huang on 23/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit

class ActivityCellViewModel {
	private let fetcher: ActivityDataFetcher!
	private let item: ActivityItem
	private let isFirstItem: Bool

	var updateHandler: ((UIImage)->())?

	init(_ item: ActivityItem, fetcher: ActivityDataFetcher, isFirstItem: Bool = false) {
		self.item = item
		self.fetcher = fetcher
		self.isFirstItem = isFirstItem
	}

	var title: String  {
		return item.title
	}

	// string representation of publish date in expected output format
	var publishedDateString : String {
		return ActivityCellViewModel.dateFormatter.string(from: item.pubDate)
	}

	var imageURL: URL?  {
		if isFirstItem, let urlString = item.enclosure?.link {
			return URL(string: urlString)
		} else {
			return item.thumbnail
		}
	}

	private static let dateFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
		return dateFormatter
	}()

	func loadImage() {
		// some item may not have a image url
		guard let imageURLToLoad = imageURL else {
			return
		}

		// fetch image
		fetcher.fetchImage(imageURLToLoad) { result in
			switch result {
			case let .success(image):
				self.updateHandler?(image)
			case let .failure(error):
				print("Error fetching image: \(error)")
			}
		}
	}
}
