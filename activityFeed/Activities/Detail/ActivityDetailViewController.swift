//
//  WebViewController.swift
//  activityFeed
//
//  Created by Ge Huang on 21/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

	@IBOutlet weak var webView: WKWebView!
	var linkURL: URL!

	override func viewDidLoad() {
		super.viewDidLoad()
		webView.load(URLRequest(url: linkURL!))
	}
}
