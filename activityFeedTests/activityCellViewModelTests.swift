//
//  activityCellViewModelTests.swift
//  activityFeedTests
//
//  Created by Ge Huang on 27/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import XCTest
@testable import activityFeed

class activityCellViewModelTests: XCTestCase {

	func testFetchingCellImage() {

		let expect = expectation(description: "Expect the cell image to be fetched")

		let dataFetcher = ActivityDataFetcher(webClient: MockWebClient())
		let item = ActivityItem(title: "item",
								pubDate: Date(),
								link: URL(string: "http://www.abc.net.au")!,
								enclosure: nil,
								thumbnail: URL(string: "http://www.abc.net.au/news/image/10166710-4x3-140x105.jpg"))

		let viewModel = ActivityCellViewModel(item, fetcher: dataFetcher)

		viewModel.updateHandler = { _ in
			expect.fulfill()
		}

		viewModel.loadImage()

		waitForExpectations(timeout: 5)
	}
}
