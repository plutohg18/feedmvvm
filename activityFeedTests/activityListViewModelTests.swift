//
//  activityListViewModelTests.swift
//  activityFeedTests
//
//  Created by Ge Huang on 22/11/18.
//  Copyright © 2018 Ge Huang. All rights reserved.
//

import XCTest
@testable import activityFeed

class activityListViewModelTests: XCTestCase {

	func testListViewFectching() {
		let expect = expectation(description: "Expect the list view items to be fetched")

		let dataFetcher = ActivityDataFetcher(webClient: MockWebClient())
		let viewModel = ActivityListViewModel(dataFetcher)
		viewModel.refreshHandler = {
			expect.fulfill()
		}

		viewModel.refreshData()

		waitForExpectations(timeout: 5)

		XCTAssertEqual(viewModel.itemsCount(), 10)
	}

	func testPullToRefresh() {
		var expect = expectation(description: "Expect the list view items to be fetched")

		let dataFetcher = ActivityDataFetcher(webClient: MockWebClient())
		let viewModel = ActivityListViewModel(dataFetcher)
		viewModel.refreshHandler = {
			expect.fulfill()
		}

		viewModel.refreshData()

		waitForExpectations(timeout: 5)

		expect = expectation(description: "Expect pull to refresh to refresh data")

		viewModel.handleRefreshControl()

		waitForExpectations(timeout: 5)

		XCTAssertEqual(viewModel.itemsCount(), 10)
	}
}
